package objpool

import "time"

type PoolConfig struct {
	Max           int           //obj最大个数
	Retry         int           //错误重试次数
	RetryInterval time.Duration //重试间隔
	ObjFactory    func() (interface{}, error)
}
