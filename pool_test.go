package objpool

import (
	"log"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Llongfile)
	pool := NewObjPool(PoolConfig{
		Max:           5,
		Retry:         3,
		RetryInterval: time.Second,
		ObjFactory: func() (interface{}, error) {
			return new(int), nil
		},
	})
	group := sync.WaitGroup{}
	group.Add(30)
	for i := 0; i < 30; i++ {
		go func() {
			group.Done()
			group.Wait()
			a := pool.Get()
			b, _ := a.(*int)
			t.Logf("获取对象:%d", *b)
			// time.Sleep(time.Second * 3)
			*b += 10
			time.Sleep(time.Duration(rand.Int()) % 1000 * time.Millisecond)
			pool.Put(b)
		}()
	}
	time.Sleep(time.Millisecond * 10)
	pool.Lock()
	t.Log("外部锁定")
	time.Sleep(2 * time.Second)
	t.Log("外部解锁")
	pool.Unlock()
	pool.Reset()

	time.Sleep(2 * time.Second)
}

func BenchmarkGetSet(b *testing.B) {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Llongfile)
	pool := NewObjPool(PoolConfig{
		Max:           0,
		Retry:         3,
		RetryInterval: time.Second,
		ObjFactory: func() (interface{}, error) {
			return new(int), nil
		},
	})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {

		a := pool.Get()
		b, _ := a.(*int)
		// b.Logf("获取对象:%d", *b)
		// time.Sleep(time.Second * 3)
		*b += 10
		pool.Put(b)

	}
}
